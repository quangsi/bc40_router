import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import Header from "./Components/Header/Header";
import HomePage from "./Pages/HomePage/HomePage";
import LifeCyclePage from "./Pages/LifeCyclePage/LifeCyclePage";
import LoginPage from "./Pages/LoginPage/LoginPage";
import MovieListPage from "./Pages/MovieListPage/MovieListPage";
import DemoHook from "./Pages/DemoHook/DemoHook";
import Ex_Movie_Hook from "./Pages/Ex_Phone_Hook/Ex_Movie_Hook";
import DetailPage from "./Pages/DetailPage/DetailPage";
import LifeCycleHook from "./Pages/LifeCycleHook/LifeCycleHook";
import TaiXiuPage from "./Pages/TaiXiuPage/TaiXiuPage";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Header />
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="/login" element={<LoginPage />} />
          <Route path="/life-cycle" element={<LifeCyclePage />} />
          <Route path="/movie-list" element={<MovieListPage />} />
          <Route path="/demo-hook" element={<DemoHook />} />
          <Route path="/movie-hook" element={<Ex_Movie_Hook />} />
          <Route path="/movie/:id" element={<DetailPage />} />
          <Route path="/life-cycle-hook" element={<LifeCycleHook />} />
          <Route path="/tai-xiu" element={<TaiXiuPage />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
// http://localhost:3000/home
