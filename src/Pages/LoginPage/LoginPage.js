import React, { Component } from "react";
import axios from "axios";
export default class LoginPage extends Component {
  state = {
    username: "alice",
    password: "123",
  };
  handleOnchangeLogin = (e) => {
    console.log("event", e.target.name);
    let { value, name } = e.target;
    this.setState({ [name]: value });
  };
  handleLogin = () => {
    // james james
    axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyNguoiDung/DangNhap",
      method: "POST",
      data: {
        taiKhoan: this.state.username,
        matKhau: this.state.password,
      },
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MCIsIkhldEhhblN0cmluZyI6IjEwLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDMwNDAwMDAwMCIsIm5iZiI6MTY2NTY4MDQwMCwiZXhwIjoxNjk0NDUxNjAwfQ.sBqNvFEzAEqAZuxinnH_gzedfmLxPTf7WONjIlV-Q7U",
      },
    })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  render() {
    return (
      <div className="container">
        <h1>LoginPage</h1>
        <div className="form-group">
          <input
            onChange={this.handleOnchangeLogin}
            value={this.state.username}
            type="text"
            className="form-control"
            name="username"
            placeholder="Username"
          />
        </div>
        <div className="form-group">
          <input
            onChange={this.handleOnchangeLogin}
            value={this.state.password}
            type="text"
            className="form-control"
            name="password"
            placeholder="Password"
          />
        </div>
        <button onClick={this.handleLogin} className="btn btn-success">
          Login
        </button>
      </div>
    );
  }
}

let user = {
  name: "alice",
  age: 2,
};
// user.name = "bob";
let key = "name";

user.name = "bob";
user["name"] = "bob";
user[key] = "bob";
