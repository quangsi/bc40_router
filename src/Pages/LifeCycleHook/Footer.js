import React, { memo } from "react";

function Footer({ like, handlePlusLike }) {
  console.log("Footer render");
  return (
    <div className="text-white p-5 bg-success">
      <strong>Like:{like}</strong>
      <button onClick={handlePlusLike} className="btn btn-primary">
        plus like
      </button>
    </div>
  );
}
export default memo(Footer);
