import { Button } from "antd";
import React, { useCallback, useEffect, useMemo, useState } from "react";
import Footer from "./Footer";

export default function LifeCycleHook() {
  let [number, setNumber] = useState(10);
  let [like, setLike] = useState(10);
  // useEffect : didMount - didUpdate - willUnmount
  useEffect(() => {
    console.log("Did mount - UseEffect with dependency list : number");
  }, [number]); // chạy lại useEffect nếu number update giá trị
  //   [] => dependency list => nếu ko có []  => vô dụng ( đụng gì cũng chạy lại)
  useEffect(() => {
    console.log("Did mount - UseEffect with dependency list : like");
  }, [like]);
  useEffect(() => {
    return () => {
      //  sẽ được thực thi khi willUnmount xảy ra
      console.log("Tạm biệt");
    };
  }, []);
  let handleIncrease = () => setNumber(number + 1);
  let handleDecrease = () => setNumber(number - 1);
  let handlePlusLike = useCallback(() => {
    setLike(like + 1);
  }, [like]);

  //   useCallback =>áp dụng cho funciton

  let sum = useMemo(() => {
    return like + 100;
  }, [like]);
  //   useMemo => áp dụng cho giá trị => lưu kết quả tính toán

  console.log("render");
  return (
    <div className="text-center">
      <h2>sum:{sum}</h2>
      <Button onClick={handleDecrease} type="primary" danger>
        -
      </Button>
      <strong className="mx-2">number: {number}</strong>
      <Button onClick={handleIncrease} type="primary">
        +
      </Button>
      <br />
      <strong className="mx-2">Like: {like}</strong>
      <Button onClick={handlePlusLike} type="dashed">
        {" "}
        Plus like
      </Button>
      <br />
      <Footer handlePlusLike={handlePlusLike} like={like} />
    </div>
  );
}
