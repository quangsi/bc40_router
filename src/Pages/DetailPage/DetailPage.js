import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

export default function DetailPage() {
  let [movie, setMovie] = useState({});
  useEffect(() => {
    axios({
      url: `https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayThongTinPhim?MaPhim=${id}`,
      method: "GET",
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MCIsIkhldEhhblN0cmluZyI6IjEwLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDMwNDAwMDAwMCIsIm5iZiI6MTY2NTY4MDQwMCwiZXhwIjoxNjk0NDUxNjAwfQ.sBqNvFEzAEqAZuxinnH_gzedfmLxPTf7WONjIlV-Q7U",
      },
    })
      .then((res) => {
        setMovie(res.data.content);
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let { id } = useParams();

  return (
    <div>
      <div>{JSON.stringify(movie)}</div>
    </div>
  );
}
