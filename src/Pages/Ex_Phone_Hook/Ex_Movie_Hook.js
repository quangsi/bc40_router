import React, { useEffect, useState } from "react";
import axios from "axios";
import ItemMovie from "./ItemMovie";

export default function Ex_Movie_Hook() {
  // useEffect ~ life cycle : didMount , didUpdate, willUnmount
  const [movieArr, setMovieArr] = useState([]);
  const [detail, setDetail] = useState({});
  useEffect(() => {
    axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01",
      method: "GET",
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MCIsIkhldEhhblN0cmluZyI6IjEwLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDMwNDAwMDAwMCIsIm5iZiI6MTY2NTY4MDQwMCwiZXhwIjoxNjk0NDUxNjAwfQ.sBqNvFEzAEqAZuxinnH_gzedfmLxPTf7WONjIlV-Q7U",
      },
    })
      .then((res) => {
        console.log(res.data.content);
        setMovieArr(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  let renderMovieList = () => {
    return movieArr.map((item) => {
      return <ItemMovie movie={item} />;
    });
  };

  return (
    <div>
      <h2>Ex_Movie_Hook</h2>
      <div className="row">{renderMovieList()}</div>
    </div>
  );
}
