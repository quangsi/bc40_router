import React from "react";
import { Card, Button } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;
export default function ItemMovie({ movie }) {
  // let { movie } = props;
  return (
    <div className="col-2 p-2">
      <Card
        hoverable
        style={{
          width: 240,
          height: "100%",
        }}
        cover={
          <img
            style={{ height: "300px", objectFit: "cover" }}
            alt="example"
            src={movie.hinhAnh}
          />
        }
      >
        <NavLink to={`/movie/${movie.maPhim}`}>
          <Button type="primary">Xem chi tiết</Button>
        </NavLink>
        <Meta title={movie.tenPhim} description="www.instagram.com" />
      </Card>
    </div>
  );
}
