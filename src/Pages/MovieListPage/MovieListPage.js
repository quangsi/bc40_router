import React, { Component } from "react";
import axios from "axios";
import { Card } from "antd";
const { Meta } = Card;
export default class MovieListPage extends Component {
  state = {
    movieArr: [],
  };
  componentDidMount() {
    axios({
      url: "https://movienew.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01",
      method: "GET",
      headers: {
        TokenCybersoft:
          "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MCIsIkhldEhhblN0cmluZyI6IjEwLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDMwNDAwMDAwMCIsIm5iZiI6MTY2NTY4MDQwMCwiZXhwIjoxNjk0NDUxNjAwfQ.sBqNvFEzAEqAZuxinnH_gzedfmLxPTf7WONjIlV-Q7U",
      },
    })
      .then((res) => {
        this.setState({
          movieArr: res.data.content,
        });
        console.log(this.data.content);
        console.log(this.state.movieArr);
      })
      .catch((err) => {
        console.log(err);
      });
  }
  renderMovieList = () => {
    return this.state.movieArr.map((item) => {
      return (
        <Card
          className="col-2"
          hoverable
          style={{
            width: 240,
          }}
          cover={
            <img
              alt="example"
              src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png"
            />
          }
        >
          <Meta title="Europe Street beat" description="www.instagram.com" />
        </Card>
      );
    });
  };

  render() {
    return <div className="row">{this.renderMovieList()}</div>;
  }
}
