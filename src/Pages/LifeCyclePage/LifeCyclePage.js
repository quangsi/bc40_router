import React, { Component } from "react";
import Header_Life_Cycle from "./Header_Life_Cycle";

export default class LifeCyclePage extends Component {
  state = {
    number: 1,
  };
  componentDidMount() {
    // chỉ chạy 1 lần duy nhất SAU khi  render() lần đầu
    console.log("Did mount - Parent");
    let count = 1000;
    // this.countDown = setInterval(() => {
    //   count--;
    //   console.log("count down", count);
    // }, 1000);
  }
  componentDidUpdate(preProps, preState) {
    // console.log(`   preState`, preState);
    // console.log(`   preProps`, preProps);
    console.log("did update");
    //  tự động được gọi sau khi render chạy xong
  }
  componentWillUnmount() {
    console.log("Will unmount - Parent");
    // clearInterval(this.countDown);
  }
  shouldComponentUpdate(nextProps, nextState) {
    // return về true || hoặc
    // return true => chạy method render()
    // return false => ko render lại
    console.log({ nextProps, nextState });
    if (nextState.number == 5) {
      return false;
    } else {
      return true;
    }
  }
  render() {
    // render : chạy lại khi setState
    console.log("Re-Render - Parent");
    return (
      <div className="text-center pt-5">
        <h1>LifeCyclePage</h1>
        <Header_Life_Cycle />
        <div>
          <button
            onClick={() => {
              this.setState({
                number: this.state.number - 1,
              });
            }}
            className="btn btn-danger"
          >
            -
          </button>
          <strong className="mx-3">{this.state.number}</strong>
          <button
            onClick={() => {
              this.setState({
                number: this.state.number + 1,
              });
            }}
            className="btn btn-success"
          >
            +
          </button>
        </div>
      </div>
    );
  }
}
