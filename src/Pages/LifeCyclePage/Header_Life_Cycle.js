import React, { PureComponent } from "react";

export default class Header_Life_Cycle extends PureComponent {
  componentDidMount() {
    console.log("Did mount - Header");
  }
  render() {
    console.log("Re-render - Header");
    return (
      <div className="p-5 bg-warning ">
        <h2>Header_Life_Cycle</h2>
      </div>
    );
  }
}
