import React, { useState } from "react";

// KHÔNG dùng THIS
export default function DemoHook() {
  let [number, setNumber] = useState(100);
  //   setNumber ~ this.setState
  console.log(number);
  let handleIncrease = () => {
    setNumber(number - 1);
  };
  let handleDecrease = () => {
    setNumber(number + 1);
  };

  return (
    <div className="text-center">
      <h2>DemoHook</h2>
      <div>
        <button onClick={handleDecrease} className="btn btn-danger">
          -
        </button>
        <strong className="mx-2">{number}</strong>
        <button onClick={handleIncrease} className="btn btn-success">
          +
        </button>
      </div>
    </div>
  );
}
