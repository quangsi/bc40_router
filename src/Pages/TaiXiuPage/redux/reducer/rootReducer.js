import { combineReducers } from "redux";
import gameReducer from "./gameReducer";
export const rootReducerGame = combineReducers({ gameReducer });
