import React from "react";
import { useSelector } from "react-redux";
export default function XucXac() {
  let { mangXucXac } = useSelector((state) => {
    return state.gameReducer;
  });
  let renderMangXucXac = () => {
    return mangXucXac.map((item, index) => {
      return (
        <img
          style={{
            width: 100,
          }}
          key={index}
          src={item.img}
          alt=""
        />
      );
    });
  };

  return (
    <div className="d-flex container justify-content-between align-items-center  pt-5">
      <button className="btn btn-danger p-5">Tài</button>
      <div>{renderMangXucXac()}</div>
      <button className="btn btn-dark p-5">Xỉu</button>
    </div>
  );
}
