import React from "react";
import bg_game from "../../assets/bgGame.png";
import "./game.css";
import KetQua from "./KetQua";
import XucXac from "./XucXac";
export default function TaiXiuPage() {
  return (
    <div
      className="game_container"
      style={{
        backgroundImage: `url(${bg_game})`,
        width: "100vw",
        height: "100vh",
      }}
    >
      <XucXac />
      <KetQua />
    </div>
  );
}
// redux
// npm i redux react-redux
