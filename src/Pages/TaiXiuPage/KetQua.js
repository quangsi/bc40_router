import React from "react";
import { useSelector } from "react-redux";

export default function KetQua() {
  let { luaChon, tongSoBanThang, tongSoLuotChoi } = useSelector((state) => {
    return state.gameReducer;
  });
  return (
    <div className="text-center  pt-5">
      <button className="btn btn-warning px-5  py-2">Play Game</button>
      <h2>{luaChon}</h2>
      <h2>Tổng số bàn thắng {tongSoBanThang}</h2>
      <h2>Tổng số lượt chơi {tongSoLuotChoi}</h2>
    </div>
  );
}
